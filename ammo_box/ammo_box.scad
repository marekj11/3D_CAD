// Number segments of circle
$fn = 100;

// Count of ammo in box
cntX = 5;
cntY = 4;

// Height of ammo which is over the top of box
vOver = 9;

// Tolerance of diameter
tol = 1;

// Diameter of bottom side ammo
D = 10;
// Whole height of ammo
v = 35;

// Thickness of border
borderThick = 2;
// Space between cartridge
spaceBetween = 2;
// Thickness of bottom side under projectile
floorThick = 2;

// cartridge height
cartridgeHeight = 25;
// projectile height
projectileHeight = 10;
// projectile diameter
projectileD = 9;
// spike projectile diameter
projectileTopD = 6;

sizeX = cntX * (D + tol / 2) + (cntX - 1) * spaceBetween + 2 * borderThick;
sizeY = cntY * (D + tol / 2) + (cntY - 1) * spaceBetween + 2 * borderThick;
sizeZ = v - vOver + floorThick;
top = sizeZ + vOver;
ammo_height = cartridgeHeight + projectileHeight;

difference(){
    cube([sizeX, sizeY, sizeZ]);
    translate([(D / 2 + borderThick + spaceBetween / 2), -(D / 2 + borderThick + spaceBetween / 2) + sizeY, ammo_height + floorThick])
        rotate([180, 0, 0])
            ammo_matrix(cntX, cntY, D + spaceBetween, D + spaceBetween, cartridgeHeight, projectileHeight, projectileD, projectileTopD, D);
}

translate([0, sizeY + 10, 0])difference(){
    cube([sizeX + 2 * borderThick + tol, sizeY + 2 * borderThick + tol, vOver + borderThick + 3]);
    translate([borderThick + tol / 4, borderThick + tol / 4, borderThick])cube([sizeX, sizeY, vOver + 3]);
}

module ammo_matrix(cx, cy, spx, spy, cartridgeHeight, projectileHeight, projectileD, projectileTopD, D){
    union(){
        for(x = [0:cx-1])
            for(y = [0:cy-1]){
                translate([x * spx, y * spy, 0])
                    ammo(cartridgeHeight, projectileHeight, projectileD, projectileTopD, D);
            }
    }
}

module ammo(ch, ph, sD, sTD, D){
    union(){
        cylinder(d = D + tol / 4, h = ch);
        translate([0, 0, ch])
            cylinder(d1 = sD + tol / 4, d2 = sTD, h = ph);
    }
}
