// Number segments of circle
$fn = 100;

// Number of card boxes side by side
cnt = 3;

// Width of card + 1mm space
width = 45;
// Depth of card (card is mated with front site of box
depth = 60;
// Height of card deck
height = 30;
// Pitch of bottom size in degrees
floorPitch = 5;

// Thickness of sides
borderThick = 2;
// Thickenss of middle partitions
partitionThick = 1.5;
// Space over card in the holder
heightSpace = 10;

// Finger picking
fingerDepth = depth * 0.6;
fingerWidth = width * 0.6;

// Angle of cropped sides
croppedAngle = atan((height + heightSpace + borderThick - (depth + borderThick) * tan(floorPitch + 5)) / (depth+borderThick));

// Whole holder size
wWidth = width * cnt + borderThick * 2 + partitionThick * (cnt - 1);
wDepth = depth + borderThick;
wHeight = height + borderThick + heightSpace;

difference(){
    union(){
        difference(){
            cube([wWidth,wDepth,wHeight]);
            for(n = [0:cnt-1]){
                union(){
                    translate([borderThick + n * (width + partitionThick), 0, borderThick]) {
                        cube([width, depth, height + heightSpace]);
                    }
                }
            }
        }
        intersection(){
            cube([wWidth, wDepth, wHeight]);
            rotate([-floorPitch, 0, 0])
                translate([0, -wDepth, -wHeight+borderThick+depth*tan(floorPitch)])
                    cube([wWidth, wDepth*2, wHeight]);
        }
    }
    for(n = [0:cnt-1]){
        union(){
            finger(borderThick + width / 2 - fingerWidth / 2 + n * (width + partitionThick), 0, 0, wHeight);
        }
    }
    rotate([croppedAngle + 5, 0, 0])
        translate([0, 0, 5 + (borderThick + depth * tan(floorPitch))])
            cube([wWidth, wDepth + 20, wHeight]);
}

module finger(x,y,z, h){
    union(){
        translate([x, y, z]){
            union(){
                cube([fingerWidth, fingerDepth, h]);
                translate([fingerWidth / 2, fingerDepth, 0])
                    cylinder(h = h, r = fingerWidth / 2);
            }
        }
    }
}
